import os
from fastapi import FastAPI, Depends
from functools import lru_cache
from starlette.exceptions import HTTPException as StarletteHTTPException
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import PlainTextResponse
import config
from dotenv import load_dotenv
from routers import institutions, balance, login

import os

from fastapi import  requests
from schemas import ListaInstituciones

app = FastAPI()
load_dotenv()
BELVO_API_URL = 'https://sandbox.belvo.com'
BELVO_CLIENT_ID = os.getenv('BELVO_CLIENT_ID')
BELVO_SECRET_KEY = os.getenv('BELVO_SECRET_KEY')
app.include_router(institutions.router)
app.include_router(balance.router)
app.include_router(login.router)


origins = [
    "http://localhost:3000",
    "https://prueba-front-fca26032ad3e.herokuapp.com"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    print(f"{repr(exc)}")
    return PlainTextResponse(str(exc.detail), status_code=exc.status_code)


@lru_cache()
def get_settings():
    return config.Settings()


@app.get("/")
def read_root(settings: config.Settings = Depends(get_settings)):
    print(settings.app_name)
    return "Hello World"
