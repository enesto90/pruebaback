from dotenv import load_dotenv
from base64 import b64encode
from schemas import *
from modules import token
import requests
import os
import json
import rstr



load_dotenv()
BELVO_API_URL = 'https://sandbox.belvo.com'
BELVO_CLIENT_ID = os.getenv('BELVO_CLIENT_ID')
BELVO_SECRET_KEY = os.getenv('BELVO_SECRET_KEY')

def crearLink(id: str):
    headers = token.crear_headers_autenticacion()

    bank_response = requests.get(f'{BELVO_API_URL}/api/institutions/'+str(id), headers=headers)
    bank_details = bank_response.json()
    form_fields = bank_details["form_fields"]
    fields = {}
    fields["institution"]=bank_details['name']
    print(form_fields)
    for i, field in enumerate(form_fields):
        if("validation" in field):
            regex = (field["validation"])
            value = rstr.xeger(regex)
            name=field["name"]
            fields[name]=value

    headers = token.crear_headers_autenticacion()
    response = requests.post(f'{BELVO_API_URL}/api/links/', headers=headers,
    json=fields)
    link = response.json()
    print (link)
    if response.status_code == 201:
        return obtenerLink( bank_details['name'])
        # Suponiendo que la respuesta de Belvo es una lista en el campo 'data'
    else:
        return {"error": "No se pudo obtener la información de las instituciones"}

def existeLink(name: str):
    headers = token.crear_headers_autenticacion()
    response = requests.get(f'{BELVO_API_URL}/api/links/', headers=headers)
    link_details_raw = response.json()
    link_details_list=ListaLinkDetails(linkDetails=[LinkDetails(**inst) for inst in link_details_raw['results']])
    if link_details_raw["count"]==0:
        print("No se hay links")
        return False
    else:
        for detail in link_details_list.linkDetails:
            print("Buscando links")
            print(detail)
            if detail.institution == name:
                return True
        return False

def obtenerLink(name: str):
    headers = token.crear_headers_autenticacion()
    response = requests.get(f'{BELVO_API_URL}/api/links/', headers=headers)
    link_details_raw = response.json()
    link_details_list=ListaLinkDetails(linkDetails=[LinkDetails(**inst) for inst in link_details_raw['results']])
    if link_details_raw["count"]==0:
        return False
    for detail in link_details_list.linkDetails:
        if detail.institution == name:
            return detail.id
    return False



