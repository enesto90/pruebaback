from dotenv import load_dotenv
from base64 import b64encode
import os


load_dotenv()
BELVO_API_URL = 'https://sandbox.belvo.com'
BELVO_CLIENT_ID = os.getenv('BELVO_CLIENT_ID')
BELVO_SECRET_KEY = os.getenv('BELVO_SECRET_KEY')

def crear_headers_autenticacion():
    credenciales = f"{BELVO_CLIENT_ID}:{BELVO_SECRET_KEY}"
    credenciales_codificadas = b64encode(credenciales.encode()).decode()
    headers = {
        'Authorization': f'Basic {credenciales_codificadas}',
        'Content-Type': 'application/json'
    }
    return headers

