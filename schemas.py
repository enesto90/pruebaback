from typing import Optional, List
from pydantic import BaseModel


class Institucion(BaseModel):
    id: int
    name: str
    display_name: str
    primary_color: str
    logo: Optional[str]

class ListaInstituciones(BaseModel):
    instituciones: List[Institucion]

class RegisterLink(BaseModel):
    name: str
    username: str
    password:str
    id:str

class LinkDetails(BaseModel):
    institution:str
    id:str

class ListaLinkDetails(BaseModel):
    linkDetails: List[LinkDetails]

class BalanceDetails(BaseModel):
    id: str
    type: str
    status: str
    amount: float
    category: Optional[str]
    description: Optional[str]
    created_at: str

class BalanceResponse(BaseModel):
    balance: float
    income: float
    expense: float
    balance_details: List[BalanceDetails]