from pydantic_settings import BaseSettings # NEW


class Settings(BaseSettings):
    app_name: str = "Prueba"

    class Config:
        env_file = ".env"
