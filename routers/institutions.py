import json
import os
import requests

from dotenv import load_dotenv
from fastapi import APIRouter
from schemas import ListaInstituciones, Institucion
from modules import token

load_dotenv()

router = APIRouter(prefix="/api", tags=["institutions"])

BELVO_API_URL = 'https://sandbox.belvo.com'
BELVO_CLIENT_ID = os.getenv('BELVO_CLIENT_ID')
BELVO_SECRET_KEY = os.getenv('BELVO_SECRET_KEY')

@router.get("/instituciones")
async def listar_instituciones():
    headers = token.crear_headers_autenticacion()
    response = requests.get(f'{BELVO_API_URL}/api/institutions/', headers=headers)
    if response.status_code == 200:
        # Mapea la respuesta JSON a tu modelo Pydantic
        instituciones_raw = response.json()
        # Suponiendo que la respuesta de Belvo es una lista en el campo 'data'
        instituciones_mapeadas = ListaInstituciones(instituciones=[Institucion(**inst) for inst in instituciones_raw['results']])
        return instituciones_mapeadas
    return {"error": "No se pudo obtener la información de las instituciones"}
