import json
import os
import requests

from dotenv import load_dotenv
from fastapi import APIRouter
from schemas import BalanceDetails, BalanceResponse
from modules import token, link
import math

load_dotenv()

router = APIRouter(prefix="/api", tags=["balance"])

BELVO_API_URL = 'https://sandbox.belvo.com'
BELVO_CLIENT_ID = os.getenv('BELVO_CLIENT_ID')
BELVO_SECRET_KEY = os.getenv('BELVO_SECRET_KEY')

@router.get("/balance")
async def listar_Transacciones(id:str, name:str):
    linkId=''
    if not link.existeLink(name):
        print('Creando link')
        linkId=link.crearLink(id)
    else:
        print('Obteniendo link')
        linkId=link.obtenerLink(name)

    headers = token.crear_headers_autenticacion()
    encoded_data ={
        "link": linkId,
        "date_from": "1990-10-10",
        "date_to": "2024-02-02"
    }
    balance_response = requests.post(f'{BELVO_API_URL}/api/transactions/', headers=headers,
                            json=encoded_data)
    print (encoded_data)
    balance_json = balance_response.json()
    print(balance_json)
    balance_details = []
    for transaction in balance_json:
        single = BalanceDetails(id=transaction["id"],
                                type=transaction["type"],
                                status=transaction["status"],
                                amount=transaction["amount"],
                                category=transaction["category"],
                                description=transaction["description"],
                                created_at=transaction["created_at"])
        balance_details.append(single)

    incomes_nodes = list(filter(lambda item: item.get("type") == "INFLOW", balance_json))
    expenses_nodes = list(filter(lambda item: item.get("type") == "OUTFLOW", balance_json))
    sum_inflow = math.fsum(income_node["amount"] for income_node in incomes_nodes)
    sum_outflow = math.fsum(expense_node["amount"] for expense_node in expenses_nodes)

    balance = sum_inflow - sum_outflow

    balance_response = BalanceResponse(balance=balance, balance_details=balance_details, income=sum_inflow,
                                    expense=sum_outflow)

    return balance_response
    return {"error": "No se pudo obtener la información de las instituciones"}
