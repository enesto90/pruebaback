from fastapi import FastAPI, HTTPException, Form, status, APIRouter, responses
from passlib.context import CryptContext
from modules import db
from pydantic import BaseModel

class User(BaseModel):
    email: str
    password: str
    nombre: str
class Login(BaseModel):
    email: str
    password: str
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
router = APIRouter(prefix="/auth", tags=["log"])

@router.post("/login")
async def login(login:Login):
    dbcon = await db.get_db_connection()
    usuario = await dbcon.fetchrow("SELECT * FROM usuarios WHERE correo = $1", login.email)
    await dbcon.close()
    print(usuario)
    if (usuario["correo"]== login.email and usuario["contraseña"]== login.password):
        # Login exitoso, maneja según necesites
        return responses.JSONResponse(content = {'Login' : 'Login generado correctamente'},status_code=200)
    else:
        raise HTTPException(status_code=400, detail="Correo o contraseña inválidos")
    

@router.post("/registro")
async def register(user: User):
    dbcon = await db.get_db_connection()
    usuario_existente = await dbcon.fetchrow("SELECT * FROM usuarios WHERE correo = $1", user.email)

    if usuario_existente:
        await dbcon.close()
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="El correo ya está registrado")

    await dbcon.execute("INSERT INTO usuarios (nombre, correo, contraseña) VALUES ($1, $2, $3)", user.nombre, user.email, user.password)
    await dbcon.close()

    return {"mensaje": "Usuario registrado exitosamente"}